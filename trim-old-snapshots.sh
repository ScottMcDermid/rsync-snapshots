#!/bin/sh
#
# Warning: This script removes directories and files. Avoid using directly.
#
# Example usage:
# ./trim-old-snapshots.sh <prefix> ./target/directory 10
#
# This example removes all but the 10 newest directories
# Only observes files starting with <prefix>
# 

if [ $# -lt 3 ]; then
  echo "Usage: $0 prefix ./target/directory <number of snapshots to keep>"
  exit 2
fi

PREFIX=$1
TARGET=$2
MAX_SNAPSHOTS=$3

if [ $MAX_SNAPSHOTS -lt 1 ]; then
  echo "Number of snapshots should be greater than 0"
  exit 2
fi

cd $TARGET
/bin/ls -1tr | grep "^${PREFIX}" | head -n -10 | xargs -d '\n' rm -rf
cd -
