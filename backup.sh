#!/bin/sh
#
# It is recommended to use one of the other wrapper functions, in order to simplify its cli usage
#
# Example usage:
# ./backup.sh OneTime Documents ~/documents ~/backups 10
#
# The optional 5th parameter is the maximum snapshots to retain in the backups directory

if [ $# -lt 4 ]; then
  echo "Usage: $0 Frequency Label ./source/directory ./target/directory"
  exit 2
fi

DEFAULT_MAX_SNAPSHOTS=99999 # default value for maximum snapshots

FREQUENCY=$1
LABEL=$2
SOURCE="$3/*" # copy contents of directory, not the directory itself
TARGET=$4
MAX_SNAPSHOTS=${5:-$DEFAULT_MAX_SNAPSHOTS} # optional argument for number of maximum snapshots

SNAPSHOT_LABEL="Backup-${FREQUENCY} ${LABEL}-$(date '+%Y-%m-%d_%H-%M')"
LABEL_DIR="${LABEL,,}" # convert label directory to lowercase
FREQUENCY_DIR="${FREQUENCY,,}" # convert frequency directory to lowercase
FULL_TARGET_DIR="${TARGET}/${FREQUENCY_DIR}/${LABEL_DIR}"

mkdir -p $FULL_TARGET_DIR
rsync -a $SOURCE "${FULL_TARGET_DIR}/${SNAPSHOT_LABEL}"

sh trim-old-snapshots.sh "Backup-${FREQUENCY} ${LABEL}-" $FULL_TARGET_DIR $MAX_SNAPSHOTS

