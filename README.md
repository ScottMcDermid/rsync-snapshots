rsync-snapshots
===============

Organize your backups by frequency and type.

## Usage
### Example
`./daily Documents ~/documents ~/backups`

Results in a snapshot stored in this directory structure:

```
backups                                                                                                                                                 
└── daily                                                                                                                                               
    └── documents                                                                                                                                       
        ├── Backup-Documents Daily-2021-01-26_15-31
        ...
```

The resulting format:

`TARGET/FREQUENCY/LABEL/SNAPSHOT`

## Shorthand

Instead of calling the `backup` script directly, it is recommended to use one of the wrapper scripts:

* `daily`
* `monthly`
* `onetime`

These are simply wrapper scripts for `backup`, which populate the `FREQUENCY` argument automatically.

## Cron

It is recommended to run these commands using cron using their respectively named intervals:

### Examples

`0 0 * * * ./daily Documents ~/documents ~/backups >/dev/null 2>&1`

`0 0 1 * * ./monthly Documents ~/documents ~/backups >/dev/null 2>&1`

## Configuration

### Number of Snapshots to Retain

Configure the number of snapshots to retain using the `MAX_SNAPSHOTS` variable within `backup.sh`
